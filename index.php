<!doctype html>
<html>
<head>
  <title>
    Pendu App: Interface d'admin
  </title>
  <style>
  * {
    list-style: none;
    font-family: helvetica, sans;
    color: #222;
    background-color: #ddd;
  }

  a {
    text-decoration: none;
    text-align: center;
    padding: 10px 30px;
    background: grey;
    color: white;
    display: inline-block;
  }

  a:hover {
    color: #222;
    background: #555;
    background: linear-gradient( #777, #333);
  }

  a:active {
    color: #000;
    background: #444;
    background: linear-gradient( #555, #2C2C2C);
    box-shadow: 1px 1px 10px black inset,
    0 1px 0 rgba( 255, 255, 255, 0.4);
  }

  ul {
    padding: 0 15px;
  }
  </style>
</head>
<body>
  <ul>
    <?php
    ini_set("display_errors","true");
    $handle = mysqli_connect("localhost","root","1234","Pendu App");
    $query = "SELECT * FROM mots";
    $result = mysqli_query($handle,$query);
    while($line = mysqli_fetch_array($result)) {
      echo "\t<li>";
      echo "[" . $line["id"] . "]";
      echo $line["mot"];
      echo "</li>\n";
    }
    ?>
  </ul>
  <div>
    <a href="create.php">Créer un mot</a>
  </div>
  <div>
    <a href="update.php">Modifier un mot</a>
  </div>
  <div>
    <a href="delete.php">Supprimer un mot</a>
  </div>
</body>
</html>
