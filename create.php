<!doctype html>
<html>
<head>
  <title>
    Pendu App: Création
  </title>
  <style>
  body {
    font-family: helvetica, sans;
    font-size: 24dip;
    color: #222;
    background-color: #ddd;
  }
  </style>
</head>
<body>
  <form action="create.php" method="post">
    <label for="mot">Nouvelle entrée</label><br>
    <input name="mot" type="text" placeholder="Insert word">
    <input type="submit">
  </form>
  <?php
  ini_set("display_errors","true");
  if (isset($_POST["mot"])&& is_string($_POST["mot"])) {
    $handle = mysqli_connect("localhost","root","1234","Pendu App");
    $query="INSERT INTO mots (mot) VALUES (\"".$_POST["mot"]."\")";
    $result= mysqli_query($handle,$query);
    if ($handle->affected_rows > 0) {
      echo "Le mot ".$_POST["mot"]." a été créé<br>";
    }
    else {
      echo "Une erreur est survenue lors de l'enregistrement du mot ".$_POST["mot"]."<br>";
    }
  }
  echo "Retour à <a href=\"index.php\">la page d'administration</a>";
  ?>
</body>
</html>
