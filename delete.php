<!doctype html>
<html>
<head>
  <title>
    Pendu App: Suppression
  </title>
  <style>
  body {
    font-family: helvetica, sans;
    font-size: 24dip;
    color: #222;
    background-color: #ddd;
  }
  </style>
</head>
<body>
  <form action="delete.php" method="post">
    <label for="id">Suppression</label><br>
    <input name="id" type="text" placeholder="Insert id">
    <input type="submit">
  </form>
  <?php
  ini_set("display_errors","true");
  if (isset($_POST["id"])&&is_numeric($_POST["id"])) {
    $handle=mysqli_connect("localhost","root","1234","Pendu App");
    $query="SELECT * FROM mots WHERE id=" . $_POST["id"];
    $result=mysqli_query($handle,$query);
    if ($result->num_rows > 0) {
      $line=mysqli_fetch_array($result);
      $mot_a_supprimer=$line["mot"];
      $query="DELETE FROM mots WHERE id=" . $_POST["id"];
      $result=mysqli_query($handle,$query);
      if($handle->affected_rows > 0) {
        echo "Mot ".$mot_a_supprimer." supprimé<br>\n";
      }
      else {
        echo "Le mot ".$mot_a_supprimer." existe mais la suppression n'a pas marché<br>\n";
      }
    }
    else {
      echo "Le mot demandé n'existe pas<br>\n";
    }
  }
echo "Retour à <a href=\"index.php\">la page d'administration</a>";
?>
</body>
</html>
